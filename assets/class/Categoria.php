<?php

include_once 'Conectar.php';
include_once 'Controles.php';

class Categoria {
	private $cod;
	private $nome;

	private $con;
	private $ct;
	private $msgerro;

	function __construct($cod="", $nome=""){
		$this->cod = $cod;
		$this->nome = $nome;

		$this->con = new Conectar();
		$this->ct = new Controles();
	}

	//GETS
	public function getMsgerro()
    {
        return $this->msgerro;
    }

	public function getCod(){
		return $this->cod;
	}

	public function getNome(){
		return $this->nome;
	}

	//SETS
	public function setMsgerro($erro){
    	$this->msgerro = $erro;
    }

	public function setCod($cod){
		$this->cod = $cod;
	}

	public function setNome($nome){
		$this->nome = $nome;
	}

	public function salvarCategoria(){
		try{
			$sql = "SELECT cod FROM categoria WHERE cod=?";

			$sqlprep = $this->con->prepare($sql);
			@$sqlprep->bindParam(1, $this->getCod(), PDO::PARAM_STR);
			$sqlprep->execute();
			$num = $sqlprep->rowCount();

			if ($num == 1) {
				echo "
					<script>
			         	alert('Código já cadastrado!');
			      	</script>
				";

			} else {

				try{

					$sql = "INSERT INTO categoria VALUES (?, ?)";

					$preparasql = $this->con->prepare($sql);

					@$preparasql->bindParam(1, $this->getCod(), PDO::PARAM_STR);
					@$preparasql->bindParam(2, $this->getNome(), PDO::PARAM_STR);

					if ($preparasql->execute())
		            {
		                echo "
		                	<script>
						        alert('Cadastro realizado com sucesso!');
						    </script>
						";

		            } else {
		            	echo "
		            		<script>
								alert('Ops, algo deu errado!');
							</script>
						";
		            }
		            
		        } catch (Exception $exc) {
					//mensagem de erro
		            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
				}
			}

		} catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
		}
	}

	public function editarCategoria(){
		try{

			$sql = "UPDATE categoria SET nome=? WHERE cod=?";

			$preparasql = $this->con->prepare($sql);

			@$preparasql->bindParam(1, $this->getNome(), PDO::PARAM_STR);
			@$preparasql->bindParam(2, $this->getCod(), PDO::PARAM_STR);

			if ($preparasql->execute())
            {
                echo "
                	<script>
				        alert('Dados editados com sucesso!');
				        location.href='categories.php';
				    </script>
				";

            } else {
            	echo "
            		<script>
						alert('Ops, algo deu errado!');
					</script>
				";
            }
            
        } catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro".$exc->getMessage() );
		}
	}

	public function deletarCategoria(){
		try{

			$sql = "DELETE FROM categoria WHERE cod=?";

			$preparasql = $this->con->prepare($sql);

			@$preparasql->bindParam(1, $this->getCod(), PDO::PARAM_STR);

			if ($preparasql->execute())
            {
                echo "
                	<script>
				        alert('Categoria excluída com sucesso!');
				        location.href='categories.php';
				    </script>
				";

            } else {
            	echo "
            		<script>
						alert('Ops, algo deu errado!');
					</script>
				";
            }
            
        } catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro".$exc->getMessage() );
		}
	}

	public function listarCategoria(){
		try{
			$sql = "SELECT * FROM categoria";
			$sqlprep = $this->con->prepare($sql);

			if ($sqlprep->execute()) {
				
				while ($linhas = $sqlprep->fetch(PDO::FETCH_BOTH)) {
					echo "
		                <tr class='data-row'>
					        <td class='data-grid-td'>
					           	<span class='data-grid-cell-content'>$linhas[nome]</span>
					        </td>
					      
					        <td class='data-grid-td'>
					           	<span class='data-grid-cell-content'>$linhas[cod]</span>
					        </td>
					      
					        <td class='data-grid-td'>
					        	<div class='actions'>
					            	<div class='action edit'><a href='editCategory.php?cod=$linhas[cod]'><span>Edit</span></a></div>
					            	<div class='action delete'><a href='deleteCategory.php?cod=$linhas[cod]'><span>Delete</span></a></div>
					        	</div>
					        </td>
					     </tr>
					";
				}

			} else {
				echo "
					<script>
						alert('Ops, algo deu errado!');
					</script>
				";
				
			}

		} catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
		}
	}

	public function carregarDadosCategoria(){
		try{
			$sql = "SELECT * FROM categoria WHERE cod=?";
			$preparasql = $this->con->prepare($sql);

			@$preparasql->bindParam(1, $this->getCod(), PDO::PARAM_STR);

			if ($preparasql->execute()) {

	          	return $linhas = $preparasql->fetch(PDO::FETCH_BOTH);

			} else {
				echo "
					<script>
						alert('Ops, algo deu errado!');
					</script>
				";
				
			}

		} catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
		}
	}

	public function optionCategoria(){
		try{
			$sql = "SELECT * FROM categoria";
			$preparasql = $this->con->prepare($sql);
			$preparasql->execute();

			while ($linhas = $preparasql->fetch(PDO::FETCH_BOTH)){

	          	echo "
					<option value='$linhas[cod]'>$linhas[nome]</option>
	          	";

			}

		} catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
		}
	}
}

?>