<?php

include_once 'Conectar.php';
include_once 'Controles.php';

class CategoriaProduto {
	private $id;
	private $prod_sku;
	private $categ_cod;

	private $con;
	private $ct;
	private $msgerro;

	function __construct($id="", $prod_sku="", $categ_cod=""){
		$this->id = $id;
		$this->prod_sku = $prod_sku;
		$this->categ_cod = $categ_cod;

		$this->con = new Conectar();
		$this->ct = new Controles();
	}

	//GETS
	public function getMsgerro()
    {
        return $this->msgerro;
    }

	public function getId(){
		return $this->id;
	}

	public function getProd_sku(){
		return $this->prod_sku;
	}

	public function getCateg_cod(){
		return $this->categ_cod;
	}

	//SETS
	public function setMsgerro($erro){
    	$this->msgerro = $erro;
    }

	public function setId($id){
		$this->id = $id;
	}

	public function setProd_sku($prod_sku){
		$this->prod_sku = $prod_sku;
	}

	public function setCateg_cod($categ_cod){
		$this->categ_cod = $categ_cod;
	}

	public function salvarCategoriaProduto(){		

		try{

			$sql = "INSERT INTO categoria_produto VALUES (NULL, ?, ?)";

			$preparasql = $this->con->prepare($sql);

			@$preparasql->bindParam(1, $this->getProd_sku(), PDO::PARAM_STR);
			@$preparasql->bindParam(2, $this->getCateg_cod(), PDO::PARAM_STR);

			if ($preparasql->execute())
            {
                echo "
                	<script>
				        alert('Cadastro realizado com sucesso!');
				    </script>
				";

            } else {
            	echo "
            		<script>
						alert('Ops, algo deu errado!');
					</script>
				";
            }
            
        } catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
		}
	}

	public function deletarCategoriaProduto(){
		try{

			$sql = "DELETE FROM categoria_produto WHERE id=?";

			$preparasql = $this->con->prepare($sql);

			@$preparasql->bindParam(1, $this->getId(), PDO::PARAM_STR);

			if ($preparasql->execute())
            {
                echo "
                	<script>
				        alert('Categoria excluída com sucesso!');
				    </script>
				";

            } else {
            	echo "
            		<script>
						alert('Ops, algo deu errado!');
					</script>
				";
            }
            
        } catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro".$exc->getMessage() );
		}
	}

	public function listarCategoriaProduto(){
		try{
			$sql = "SELECT * FROM categoria_produto INNER JOIN categoria ON categoria_produto.categ_cod=categoria.cod WHERE prod_sku=?";
			$preparasql = $this->con->prepare($sql);

			@$preparasql->bindParam(1, $this->getProd_sku(), PDO::PARAM_STR);
			$preparasql->execute();

			while ($linhas = $preparasql->fetch(PDO::FETCH_BOTH)){

				if (isset($_POST[$linhas['prod_sku'].$linhas['categ_cod']])){
					$this->setId($linhas['id']);
	      			$this->deletarCategoriaProduto();
	      			echo "<script>location.reload()</script>";
	      		}

	          	echo "
					<form method='post'>
						<div style='border: 1px solid grey; margin-bottom: 10px;'>
							<span class='data-grid-cell-content'>$linhas[nome]</span>
							<button type='submit' name='$linhas[prod_sku]$linhas[categ_cod]'>X</button>
						</div>
					</form>
	          	";

			}

		} catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
		}
	}
}

?>