<?php

include_once 'Conectar.php';
include_once 'Controles.php';
include_once 'Categoria.php';
include_once 'Categoria_produto.php';

class Produto {
	private $nome;
	private $sku;
	private $descricao;
	private $quantidade;
	private $preco;
	private $imagem;
	private $tpimagem;

	private $con;
	private $ct;
	private $categ;
	private $msgerro;

	function __construct($nome="", $sku="", $descricao="", $quantidade="", $preco="", $imagem="", $tpimagem=""){
		$this->nome = $nome;
		$this->sku = $sku;
		$this->descricao = $descricao;
		$this->quantidade = $quantidade;
		$this->preco = $preco;
		$this->imagem = $imagem;
		$this->tpimagem = $tpimagem;

		$this->con = new Conectar();
		$this->ct = new Controles();
		$this->categ = new Categoria();
		$this->categ_prod = new CategoriaProduto();
	}

	//GETS
	public function getMsgerro()
    {
        return $this->msgerro;
    }

	public function getNome(){
		return $this->nome;
	}

	public function getSku(){
		return $this->sku;
	}

	public function getDescricao(){
		return $this->descricao;
	}

	public function getQuantidade(){
		return $this->quantidade;
	}

	public function getPreco(){
		return $this->preco;
	}

	public function getImagem(){
		return $this->imagem;
	}

	public function getTpimagem(){
		return $this->tpimagem;
	}

	//SETS
	public function setMsgerro($erro){
    	$this->msgerro = $erro;
    }

	public function setNome($nome){
		$this->nome = $nome;
	}

	public function setSku($sku){
		$this->sku = $sku;
	}

	public function setDescricao($descricao){
		$this->descricao = $descricao;
	}

	public function setQuantidade($quantidade){
		$this->quantidade = $quantidade;
	}

	public function setPreco($preco){
		$this->preco = $preco;
	}

	public function setImagem($imagem){
		$this->imagem = $imagem;
	}

	public function setTpimagem($tpimagem){
		$this->tpimagem = $tpimagem;
	}

	public function salvarProduto(){
		try{

			$sql = "SELECT sku FROM produto WHERE sku=?";

			$sqlprep = $this->con->prepare($sql);
			@$sqlprep->bindParam(1, $this->getSku(), PDO::PARAM_STR);
			$sqlprep->execute();
			$num = $sqlprep->rowCount();

			if ($num == 1) {
				echo "
					<script>
			         	alert('Código SKU já cadastrado!');
			      	</script>
				";

			} else {

				try{

					$this->ct->enviarArquivo($this->getTpimagem(), 'images/product/' . $this->getImagem());

					$sql = "INSERT INTO produto VALUES (?, ?, ?, ?, ?, ?)";

					$preparasql = $this->con->prepare($sql);

					@$preparasql->bindParam(1, $this->getNome(), PDO::PARAM_STR);
					@$preparasql->bindParam(2, $this->getSku(), PDO::PARAM_STR);
					@$preparasql->bindParam(3, $this->getDescricao(), PDO::PARAM_STR);
					@$preparasql->bindParam(4, $this->getQuantidade(), PDO::PARAM_STR);
					@$preparasql->bindParam(5, $this->getPreco(), PDO::PARAM_STR);
					@$preparasql->bindParam(6, $this->getImagem(), PDO::PARAM_STR);

					if ($preparasql->execute())
		            {
		                echo "
		                	<script>
						        alert('Cadastro realizado com sucesso!');
						    </script>
						";

		            } else {
		            	echo "
		            		<script>
								alert('Ops, algo deu errado!');
							</script>
						";
		            }
		            
		        } catch (Exception $exc) {
					//mensagem de erro
		            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
				}
			}

		} catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
		}
	}

	public function editarProduto(){
		try{

			$this->ct->enviarArquivo($this->getTpimagem(), 'images/product/' . $this->getImagem());

			$sql = "UPDATE produto SET nome=?, descricao=?, quantidade=?, preco=?, imagem=? WHERE sku=?";

			$preparasql = $this->con->prepare($sql);

			@$preparasql->bindParam(1, $this->getNome(), PDO::PARAM_STR);
			@$preparasql->bindParam(2, $this->getDescricao(), PDO::PARAM_STR);
			@$preparasql->bindParam(3, $this->getQuantidade(), PDO::PARAM_STR);
			@$preparasql->bindParam(4, $this->getPreco(), PDO::PARAM_STR);
			@$preparasql->bindParam(5, $this->getImagem(), PDO::PARAM_STR);
			@$preparasql->bindParam(6, $this->getSku(), PDO::PARAM_STR);

			if ($preparasql->execute())
            {
                echo "
                	<script>
				        alert('Dados editados com sucesso!');
				        location.href='products.php';
				    </script>
				";

            } else {
            	echo "
            		<script>
						alert('Ops, algo deu errado!');
					</script>
				";
            }
            
        } catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro".$exc->getMessage() );
		}
	}

	public function deletarProduto(){
		try{

			$sql = "DELETE FROM produto WHERE sku=?";

			$preparasql = $this->con->prepare($sql);

			@$preparasql->bindParam(1, $this->getSku(), PDO::PARAM_STR);

			if ($preparasql->execute())
            {
                echo "
                	<script>
				        alert('Produto excluído com sucesso!');
				        location.href='products.php';
				    </script>
				";

            } else {
            	echo "
            		<script>
						alert('Ops, algo deu errado!');
					</script>
				";
            }
            
        } catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro".$exc->getMessage() );
		}
	}

	public function listarProduto(){
		try{
			$sql = "SELECT * FROM produto";
			$sqlprep = $this->con->prepare($sql);

			if ($sqlprep->execute()) {
				
				while ($linhas = $sqlprep->fetch(PDO::FETCH_BOTH)) {

					$this->categ_prod->setProd_sku($linhas['sku']);

					echo "
		                <tr class='data-row'>
		                	<td class='data-grid-td'>
					           	<img class='data-grid-cell-content' width='200' height='200' style='object-fit: contain;' src='images/product/$linhas[imagem]' />
					        </td>

					        <td class='data-grid-td'>
					           	<span class='data-grid-cell-content'>$linhas[nome]</span>
					        </td>
					      
					        <td class='data-grid-td'>
					           	<span class='data-grid-cell-content'>$linhas[sku]</span>
					        </td>

					        <td class='data-grid-td'>
					           	<span class='data-grid-cell-content'>R$ $linhas[preco]</span>
					        </td>

					        <td class='data-grid-td'>
					           	<span class='data-grid-cell-content'>$linhas[quantidade]</span>
					        </td>

					        <td class='data-grid-td'>				           		
			        ";
			           			$this->categ_prod->listarCategoriaProduto();
		           	echo "
					        </td>
					      
					        <td class='data-grid-td'>
					        	<div class='actions'>
					            	<div class='action edit'><a href='editProduct.php?sku=$linhas[sku]'><span>Edit</span></a></div>
					            	<div class='action delete'><a href='deleteProduct.php?sku=$linhas[sku]'><span>Delete</span></a></div>
					            	<div class='action'><a href='addCategories.php?sku=$linhas[sku]'><span>Add Categories</span></a></div>
					        	</div>
					        </td>
					     </tr>
					";
				}

			} else {
				echo "
					<script>
						alert('Ops, algo deu errado!');
					</script>
				";
				
			}

		} catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
		}
	}

	public function carregarDadosProduto(){
		try{
			$sql = "SELECT * FROM produto WHERE sku=?";
			$preparasql = $this->con->prepare($sql);

			@$preparasql->bindParam(1, $this->getSku(), PDO::PARAM_STR);

			if ($preparasql->execute()) {

	          	return $linhas = $preparasql->fetch(PDO::FETCH_BOTH);

			} else {
				echo "
					<script>
						alert('Ops, algo deu errado!');
					</script>
				";
				
			}

		} catch (Exception $exc) {
			//mensagem de erro
            $this->setMsgerro ( "Houve erro ".$exc->getMessage() );
		}
	}
}

?>